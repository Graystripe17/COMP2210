import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Defines a library of selection methods on Collections.
 *
 * @author  Winston Van (wjv0003@auburn.edu)
 * @author  Dean Hendrix (dh@auburn.edu)
 * @version 2018-01-25
 *
 */
public final class Selector {

/**
 * Can't instantiate this class.
 *
 * D O   N O T   C H A N G E   T H I S   C O N S T R U C T O R
 *
 */
   private Selector() { }


   /**
    * Returns the minimum value in the Collection coll as defined by the
    * Comparator comp. If either coll or comp is null, this method throws an
    * IllegalArgumentException. If coll is empty, this method throws a
    * NoSuchElementException. This method will not change coll in any way.
    *
    * @param coll    the Collection from which the minimum is selected
    * @param comp    the Comparator that defines the total order on T
    * @return        the minimum value in coll
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> T min(Collection<T> coll, Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty()) {
         throw new NoSuchElementException();
      }
      T minimum = coll.iterator().next();
      for (T o : coll) {
         if (comp.compare(minimum, o) > 0) {
            minimum = o;
         }
      }
      return minimum;
   }


   /**
    * Selects the maximum value in the Collection coll as defined by the
    * Comparator comp. If either coll or comp is null, this method throws an
    * IllegalArgumentException. If coll is empty, this method throws a
    * NoSuchElementException. This method will not change coll in any way.
    *
    * @param coll    the Collection from which the maximum is selected
    * @param comp    the Comparator that defines the total order on T
    * @return        the maximum value in coll
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> T max(Collection<T> coll, Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty()) {
         throw new NoSuchElementException();
      }
      T maximum = coll.iterator().next();
      for (T o : coll) {
         if (comp.compare(maximum, o) < 0) {
            maximum = o;
         }
      }
      return maximum;
   }


   /**
    * Selects the kth minimum value from the Collection coll as defined by the
    * Comparator comp. If either coll or comp is null, this method throws an
    * IllegalArgumentException. If coll is empty or if there is no kth minimum
    * value, this method throws a NoSuchElementException. This method will not
    * change coll in any way.
    *
    * @param coll    the Collection from which the kth minimum is selected
    * @param k       the k-selection value
    * @param comp    the Comparator that defines the total order on T
    * @return        the kth minimum value in coll
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> T kmin(Collection<T> coll, int k, Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty() || k <= 0) {
         throw new NoSuchElementException();
      }
      // Note that although the lists are different, the objects it refers to are the same
      List<T> coll2 = new ArrayList<T>(coll);
      java.util.Collections.sort(coll2, comp);
      Iterator<T> it = coll2.iterator();
      try {
         int duplicates = 0;
         T current = it.next();
         for (int i = 0; i < k - 1; i++) {
            T next = it.next();
            if (current == next) {
               i--;
               duplicates++;
            }
            current = next;
         }
         if (k > coll2.size() - duplicates) {
            throw new NoSuchElementException();
         }
         return current;
      } catch (NoSuchElementException e) {
         // This automatically throws a NoSuchElementException anyway
         throw e;
      }
   }


   /**
    * Selects the kth maximum value from the Collection coll as defined by the
    * Comparator comp. If either coll or comp is null, this method throws an
    * IllegalArgumentException. If coll is empty or if there is no kth maximum
    * value, this method throws a NoSuchElementException. This method will not
    * change coll in any way.
    *
    * @param coll    the Collection from which the kth maximum is selected
    * @param k       the k-selection value
    * @param comp    the Comparator that defines the total order on T
    * @return        the kth maximum value in coll
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> T kmax(Collection<T> coll, int k, Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty() || k <= 0) {
         throw new NoSuchElementException();
      }
      // Note that although the lists are different, the objects it refers to are the same
      List<T> coll2 = new ArrayList<T>(coll);
      java.util.Collections.sort(coll2, comp);
      // Reverse coll2
      List<T> coll3 = new ArrayList<T>();
      for (T o : coll2) {
         coll3.add(0, o);
      }
      Iterator<T> it = coll3.iterator();
      try {
         int duplicates = 0;
         T current = it.next();
         for (int i = 0; i < k - 1; i++) {
            T next = it.next();
            if (current == next) {
               i--;
               duplicates++;
            }
            current = next;
         }
         if (k > coll3.size() - duplicates) {
            throw new NoSuchElementException();
         }
         return current;
      } catch (NoSuchElementException e) {
         // This automatically throws a NoSuchElementException anyway
         throw e;
      }
   }


   /**
    * Returns a new Collection containing all the values in the Collection coll
    * that are greater than or equal to low and less than or equal to high, as
    * defined by the Comparator comp. The returned collection must contain only
    * these values and no others. The values low and high themselves do not have
    * to be in coll. Any duplicate values that are in coll must also be in the
    * returned Collection. If no values in coll fall into the specified range or
    * if coll is empty, this method throws a NoSuchElementException. If either
    * coll or comp is null, this method throws an IllegalArgumentException. This
    * method will not change coll in any way.
    *
    * @param coll    the Collection from which the range values are selected
    * @param low     the lower bound of the range
    * @param high    the upper bound of the range
    * @param comp    the Comparator that defines the total order on T
    * @return        a Collection of values between low and high
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> Collection<T> range(Collection<T> coll, T low, T high,
                                         Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty()) {
         throw new NoSuchElementException();
      }
      if (comp.compare(low, high) > 0) {
         throw new NoSuchElementException();
      }
      List<T> answer = new ArrayList<T>();
      for (T o : coll) {
         if (comp.compare(o, low) >= 0 && comp.compare(o, high) <= 0) {
            answer.add(o);
         }
      }
      if (answer.size() == 0) {
         throw new NoSuchElementException();
      }
      return answer;
   }


   /**
    * Returns the smallest value in the Collection coll that is greater than
    * or equal to key, as defined by the Comparator comp. The value of key
    * does not have to be in coll. If coll or comp is null, this method throws
    * an IllegalArgumentException. If coll is empty or if there is no
    * qualifying value, this method throws a NoSuchElementException. This
    * method will not change coll in any way.
    *
    * @param coll    the Collection from which the ceiling value is selected
    * @param key     the reference value
    * @param comp    the Comparator that defines the total order on T
    * @return        the ceiling value of key in coll
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> T ceiling(Collection<T> coll, T key, Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty()) {
         throw new NoSuchElementException();
      }
      // Note that although the lists are different, the objects it refers to are the same
      List<T> coll2 = new ArrayList<T>(coll);
      // Bubble sort
      for (int i = 0; i < coll2.size() - 1; i++) {
         for (int j = i + 1; j < coll2.size(); j++) {
            if (comp.compare(coll2.get(i), coll2.get(j)) > 0) {
               T temp = coll2.get(i);
               coll2.set(i, coll2.get(j));
               coll2.set(j, temp);
            }
         }
      }
      // End Bubble sort
      Iterator<T> it = coll2.iterator();
      try {
         for (int i = 0; i < coll2.size(); i++) {
            T next = it.next();
            if (comp.compare(next, key) >= 0) {
               return next;
            }
         }
         throw new NoSuchElementException();
      } catch (NoSuchElementException e) {
         // This automatically throws a NoSuchElementException anyway
         throw e;
      }
   }


   /**
    * Returns the largest value in the Collection coll that is less than
    * or equal to key, as defined by the Comparator comp. The value of key
    * does not have to be in coll. If coll or comp is null, this method throws
    * an IllegalArgumentException. If coll is empty or if there is no
    * qualifying value, this method throws a NoSuchElementException. This
    * method will not change coll in any way.
    *
    * @param coll    the Collection from which the floor value is selected
    * @param key     the reference value
    * @param comp    the Comparator that defines the total order on T
    * @return        the floor value of key in coll
    * @throws        IllegalArgumentException as per above
    * @throws        NoSuchElementException as per above
    */
   public static <T> T floor(Collection<T> coll, T key, Comparator<T> comp) {
      if (coll == null || comp == null) {
         throw new IllegalArgumentException();
      }
      if (coll.isEmpty()) {
         throw new NoSuchElementException();
      }
      // Note that although the lists are different, the objects it refers to are the same
      List<T> coll2 = new ArrayList<T>(coll);
      // Bubble sort
      for (int i = 0; i < coll2.size() - 1; i++) {
         for (int j = i + 1; j < coll2.size(); j++) {
            if (comp.compare(coll2.get(i), coll2.get(j)) > 0) {
               T temp = coll2.get(i);
               coll2.set(i, coll2.get(j));
               coll2.set(j, temp);
            }
         }
      }
      // End Bubble sort
      List<T> coll3 = new ArrayList<T>();
      for (T o : coll2) {
         coll3.add(0, o);
      }
      Iterator<T> it = coll3.iterator();
      try {
         for (int i = 0; i < coll3.size(); i++) {
            T next = it.next();
            if (comp.compare(next, key) <= 0) {
               return next;
            }
         }
         throw new NoSuchElementException();
      } catch (NoSuchElementException e) {
         // This automatically throws a NoSuchElementException anyway
         throw e;
      }
   }

}
