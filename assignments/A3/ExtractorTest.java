import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.io.FileNotFoundException;

public class ExtractorTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

   @Test public void t1() throws FileNotFoundException {
      Extractor f = new Extractor("test.txt");
      assertEquals(f.getLinesBrute().size(), 1);
   }
   
   @Test public void t2() throws FileNotFoundException {
      Extractor f = new Extractor("test.txt");
      assertEquals(f.getLinesFast().size(), 1);
   }
   
      
   @Test public void t3() throws FileNotFoundException {
      Extractor f = new Extractor("test2.txt");
      assertEquals(f.getLinesFast().size(), 1);
   }

   @Test public void t10() throws FileNotFoundException {
      Extractor f = new Extractor("input10.txt");
      assertEquals(62, f.getLinesFast().size());
   }
}
