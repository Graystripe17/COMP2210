import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Extractor.java. Implements feature extraction for collinear points in
 * two dimensional data.
 *
 * @author  Winston Van (wjv0003@auburn.edu)
 * @author  Dean Hendrix (dh@auburn.edu)
 * @version 2018-02-27
 *
 */
public class Extractor {
   
   /** raw data: all (x,y) points from source data. */
   private Point[] points;
   
   /** lines identified from raw data. */
   private SortedSet<Line> lines;
  
   private boolean interestingFile = false;

   /**
    * Builds an extractor based on the points in the file named by filename. 
    */
   public Extractor(String filename) throws FileNotFoundException {
      System.out.println(filename);
      interestingFile = (filename.equals("test_data/input10.txt") || filename.equals("test_data/input8.txt"));
      if (interestingFile) {
         System.out.println("Interesting File " + filename);
      }
      try {
         Scanner in = new Scanner(new FileReader(filename));
         Collection<Point> pcoll = new ArrayList<Point>();
         int n = in.nextInt();
         for (int i = 0; i < n; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            pcoll.add(new Point(x, y));
            if (interestingFile) {
               System.out.println(new Point(x, y));
            }
         }
         points = pcoll.toArray(new Point[]{});
         
      } catch (FileNotFoundException e) {
         System.out.println("File not found");
         throw e;
      }
   }
  
   /**
    * Builds an extractor based on the points in the Collection named by pcoll. 
    *
    * THIS METHOD IS PROVIDED FOR YOU AND MUST NOT BE CHANGED.
    */
   public Extractor(Collection<Point> pcoll) {
      points = pcoll.toArray(new Point[]{});
   }
  
   /**
    * Returns a sorted set of all line segments of exactly four collinear
    * points. Uses a brute-force combinatorial strategy. Returns an empty set
    * if there are no qualifying line segments.
    */
   public SortedSet<Line> getLinesBrute() {
      lines = new TreeSet<Line>();
      for (Point a : points) {
         for (Point b : points) {
            for (Point c : points) {
               for (Point d : points) {
                  // In case any of these are identical points
                  if (a.slopeTo(b) == Double.NEGATIVE_INFINITY
                       || a.slopeTo(c) == Double.NEGATIVE_INFINITY
                       || a.slopeTo(d) == Double.NEGATIVE_INFINITY
                       || b.slopeTo(c) == Double.NEGATIVE_INFINITY
                       || b.slopeTo(d) == Double.NEGATIVE_INFINITY
                       || c.slopeTo(d) == Double.NEGATIVE_INFINITY) {
                     continue;
                  }
                  double reference = a.slopeTo(b);
                  if (reference == a.slopeTo(c) && reference == a.slopeTo(d)) {
                     Collection<Point> newCollection = new ArrayList<Point>();
                     newCollection.add(a);
                     newCollection.add(b);
                     newCollection.add(c);
                     newCollection.add(d);
                     Line newLine = new Line(newCollection);
                     lines.add(newLine);
                     // System.out.println(newLine);
                  }
               }
            }
         }
      }
      return lines;
   }
  
   /**
    * Returns a sorted set of all line segments of at least four collinear
    * points. The line segments are maximal; that is, no sub-segments are
    * identified separately. A sort-and-scan strategy is used. Returns an empty
    * set if there are no qualifying line segments.
    */
   public SortedSet<Line> getLinesFast() {
      lines = new TreeSet<Line>();
      
      for (Point pivot : points) {
         List<Point> list = Arrays.asList(points);
         Collections.<Point>sort(list, new Comparator<Point>() {
            @Override
            public int compare(Point p1, Point p2) {
               double difference = pivot.slopeTo(p1) - pivot.slopeTo(p2);
               if (difference > 0) {
                  return 1;
               } else if (difference < 0) {
                  return -1;
               } else {
                  return 0;
               }
            }
         });
         
//          Point[] pointsBySlope = Arrays.<Point>copyOf(points, points.length);
//          Arrays.sort(points);
//          for (int referencePoint = 0; referencePoint < points.length; referencePoint++) {
//             // Sort into ascending order of slope to the reference point.
//             // Ensures that points with equal slope to the reference point
//             // will be in contiguous array indices.
//             Arrays.<Point>sort(pointsBySlope, points[referencePoint].slopeOrder);
//          }
//          
//          
         
         // Now scan the slopes in blues
         for (int x = 1; x < list.size(); x++) {
            int streak = 1;
            double streakSlope = pivot.slopeTo(list.get(x));
            for (int y = x + 1; y < list.size(); y++) {
               double pivotToY = pivot.slopeTo(list.get(y));
               // To account for the fact that the infinities are not equal to each other
               if ((pivotToY == streakSlope
                    || (pivotToY == Double.POSITIVE_INFINITY && streakSlope == Double.POSITIVE_INFINITY))
                    && y != list.size() - 1) {
                  streak++;
               } else {
                  if (y == list.size() - 1 && (pivotToY == streakSlope 
                     || (pivotToY == Double.POSITIVE_INFINITY && streakSlope == Double.POSITIVE_INFINITY))) {
                     // Gracefully end streak
                     // Add one if still collinear. If not, it's ok
                     // Either way, continue with streak chopping
                     streak++;
                  }
                  if (streak >= 3) {
                     List<Point> newlineList = new ArrayList<Point>();
                     newlineList.add(pivot);
                     for (int i = 0; i < streak; i++) {
                        newlineList.add(list.get(x + i));
                     }
                     Line potentialNewLine = new Line(newlineList);
                     boolean dupe = false;
                     for (Line l : lines) {
                        if (l.equals(potentialNewLine)) {
                           // Don't add this
                           dupe = true;
                        }
                     }
                     // If not skipped above, it's unique
                     if (!dupe) {
                        lines.add(potentialNewLine);
                     }
                     // Skip sub-dupes
                     x = y;
                  }
                  // Break out of the Y loop, which will reset the streak and move X
                  break;
               }
            }
            // Hit the end should try to end streak 
         }
      }
      // Report
      for (Line nl : lines) {
         if (interestingFile) {
            System.out.println(nl + " " + nl.length());
         }
      }
      System.out.println(lines.size() + " lines");
      return lines;
   }
   
}
