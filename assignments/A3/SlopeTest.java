import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

public class SlopeTest {
   @Test
   public void testSlopeOrder() {
      Point p1 = new Point(5, 5);
      Point p2 = new Point(10, 5);
      Point p3 = new Point(8, 7);

      assertEquals(p1.slopeOrder.compare(p2, p3), -1);
   }
   @Test
   public void testSlopeOrder2() {
      Point p1 = new Point(15, 15);
      Point p2 = new Point(20, 15);
      Point p3 = new Point(18, 17);

      assertEquals(p1.slopeOrder.compare(p2, p3), 0);
   }
}
