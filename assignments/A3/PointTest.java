import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class PointTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }

   @Test
   public void testSlopeOrder() {
      Point p1 = new Point(5, 5);
      Point p2 = new Point(10, 5);
      Point p3 = new Point(8, 7);
      assertEquals(p1.slopeTo(p2), 0, 0.2);
      assertEquals(p1.slopeTo(p3), 0.666666, 0.55);
      assertEquals(p1.slopeOrder.compare(p2, p3), -1);
   }
   
   @Test
   public void testSlopeOrder2() {
      Point p1 = new Point(15, 15);
      Point p2 = new Point(20, 15);
      Point p3 = new Point(18, 17);

      assertEquals(p1.slopeOrder.compare(p2, p3), 0);
   }
   
   @Test
   public void testSlopeOrder3() {
      Point p1 = new Point(15, 15);
      Point p2 = new Point(15, 15);
      Point p3 = new Point(15, 15);
      assertEquals(p1.slopeTo(p2), Double.NEGATIVE_INFINITY, 0.2);
      assertEquals(p1.slopeTo(p3), Double.NEGATIVE_INFINITY, 0.2);
      assertEquals(0, (int)(Double.NEGATIVE_INFINITY - Double.NEGATIVE_INFINITY), 0.2);
      assertEquals(p1.slopeOrder.compare(p2, p3), 0);
   }
}
