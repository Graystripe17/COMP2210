import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Collection;

public class LineTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }


   @Test public void t1() {
      Point p1 = new Point(5, 2);
      Point p2 = new Point(5, 2);
      Point p3 = new Point(5, 2);

      Collection c = new ArrayList<Point>();
      c.add(p1);
      c.add(p2);
      Line a = new Line(c);
      assertEquals(new Point(5, 2), a.first());
      assertEquals(new Point(5, 2), a.last());
      assertEquals(1, a.length());

   }
}
