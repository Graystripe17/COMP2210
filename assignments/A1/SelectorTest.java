import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class SelectorTest {


   /** Fixture initialization (common initialization
    *  for all tests). **/
   @Before public void setUp() {
   }


   @Test public void checkMax() {
      int[] a = {5, 2, 5, 0, 2, 3, 6};
      Assert.assertEquals("Maximus", 6, Selector.max(a));
   }
   
   @Test public void checkMin() {
      int[] b = {1, -3, 4, 2, 4};
      Assert.assertEquals("Minimus", -3, Selector.min(b));
   }

   @Test public void checkRange() {
      int[] r = {1, 2, 4, 6, 7, 8, 9, 9};
      int low = 4;
      int high = 8;
      int[] answer = {4, 6, 7, 8};
      Assert.assertArrayEquals("range", answer, Selector.range(r, low, high));
   }

   @Test public void checkCeil() {
      int[] c = {5, 8, 3, 3, 1, 6, 2, 4, 5};
      int key = 7;
      Assert.assertEquals("ceil", 8, Selector.ceiling(c, key));
   }

   @Test public void checkKMin() {
      int[] a = {1, 2, 2, 6, 9, 2, 8, 3, 7};
      int k = 4;
      Assert.assertEquals("kmin", 6, Selector.kmin(a, k));
   }

   @Test public void checkKMax() {
      int[] a = {1, 2, 2, 6, 9, 2, 8, 3, 7};
      int k = 7;
      Assert.assertEquals("kmax", 1, Selector.kmax(a, k));
   }

   @Test public void checkKMaxNormal() {
      int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
      int k = 7;
      Assert.assertEquals("kmaxNormal", 4, Selector.kmax(a, k));
   }

   @Test public void checkKMaxSmall() {
      int[] a = {3};
      int k = 1;
      Assert.assertEquals("kmaxSmall", 3, Selector.kmax(a, k));
   }

   @Test public void checkKMaxSize2() {
      int[] a = {5, 7};
      int k = 2;
      Assert.assertEquals("kmaxSize2", 5, Selector.kmax(a, k));
   }

   @Test public void checkKMaxLowerBoundary() {
      int[] a = {3, 7, 1, 5, 9};
      int k = 1;
      Assert.assertEquals("kmaxLowerBoundary", 9, Selector.kmax(a, k));
   }
}
