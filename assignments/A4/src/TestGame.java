import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;


public class TestGame implements WordSearchGame {

   private TreeSet<String> words;
   private SortedSet<String> solutions = new TreeSet<String>();
   private ArrayList<Stack<Position>> locations = new ArrayList<Stack<Position>>();
   private String[][] grid;
   private boolean[][] visited;
   private int dim;
   private int minWordLength;
   private Stack<Position> stack;


   /**
    * Drives execution.
    */
   public static void main(String[] args) {
      WordSearchGame game = WordSearchGameFactory.createGame();
      game.loadLexicon("wordfiles/words.txt");


      game.getBoard();

      System.out.print("LENT is on the board at the following positions: ");
      System.out.println(game.isOnBoard("LENT"));
      System.out.print("CAT is on the board: ");
      System.out.println(game.isOnBoard("CAT"));
//      System.out.println("TIGER isOnBoard " + game.isOnBoard("TIGER"));

      System.out.println("All words of length 10 or more: ");
      System.out.println(game.getAllValidWords(10));

   }

   /**
    * Loads Lexicon.
    */
   public void loadLexicon(String filename) {
      try {
         if (filename == null) {
            throw new IllegalArgumentException();
         }
         Scanner s = new Scanner(new File(filename));
         words = new TreeSet<String>();
         while (s.hasNext()) {
            words.add(s.next());
         }
      } catch (Exception e) {
         throw new IllegalArgumentException();
      }
   }

   /**
    * Sets the board.
    */
   public void setBoard(String[] letterArray) {
      if (letterArray == null) {
         throw new IllegalArgumentException();
      }
      int i = 0;
      dim = (int) Math.sqrt(letterArray.length);
      if (dim * dim != letterArray.length) {
         throw new IllegalArgumentException(); // Not a perfect square
      }
      grid = new String[dim][dim];
      visited = new boolean[dim][dim];
      for (int row = 0; row < dim; row++) {
         for (int col = 0; col < dim; col++) {
            grid[row][col] = letterArray[i];
            visited[row][col] = false;
            i++;
         }
      }
   }

   /**
    * Gets the board.
    */
   public String getBoard() {
      if (words == null) {
         throw new IllegalArgumentException();
      }
      StringBuilder sb = new StringBuilder();
      int fieldWidth = 4;
      for (int i = 0; i < fieldWidth * dim; i++) {
         sb.append("*");
      }
      sb.append("\n");
      for (String[] row : grid) {
         for (String entry : row) {
            sb.append(String.format("%" + fieldWidth + "s", entry));
         }
         sb.append("\n");
      }
      for (int i = 0; i < fieldWidth * dim; i++) {
         sb.append("*");
      }
      sb.append("\n");
      return sb.toString();
   }

   /**
    * Gets all valid words.
    */
   public SortedSet<String> getAllValidWords(int minimumWordLength) {
      if (minimumWordLength < 1) {
         throw new IllegalArgumentException();
      }
      if (words == null) {
         throw new IllegalStateException();
      }
      minWordLength = minimumWordLength;
      solutions = new TreeSet<String>();
      for (int y = 0; y < dim; y++) {
         for (int x = 0; x < dim; x++) {
            startDfs(new Position(x, y));
         }
      }
      return solutions;
   }

   /**
    * Gets score for words.
    */
   public int getScoreForWords(SortedSet<String> words, int minimumWordLength) {
      int points = 0;
      SortedSet<String> valid = getAllValidWords(minimumWordLength);
      for (String s : valid) {
         points += 1 + s.length() - minimumWordLength;
      }
      return points;
   }

   /**
    * Checks if word is valid.
    */
   public boolean isValidWord(String wordToCheck) {
      if (words == null) {
         throw new IllegalArgumentException();
      }
      return words.contains(wordToCheck.toLowerCase()) || words.contains(wordToCheck.toUpperCase());
   }

   /**
    * Checks if prefix is valid.
    */
   public boolean isValidPrefix(String prefixToCheck) {
      if (words == null) {
         throw new IllegalArgumentException();
      }
      String ceil = words.ceiling(prefixToCheck);
      if (ceil == null) {
         return false;
      }
      else {
         return ceil.contains(prefixToCheck);
      }
   }

   /**
    * Checks if word is on board.
    */
   public List<Integer> isOnBoard(String wordToCheck) {
      System.out.println("Checking isOnBoard for " + wordToCheck);
      // Horribly inefficient
      SortedSet<String> potentials = getAllValidWords(wordToCheck.length());
      for (String x : potentials) {
         if (x.equalsIgnoreCase(wordToCheck)) {
            // We found the right word
            for (Stack<Position> l : locations) {
               if (getWordFromStack(l).equalsIgnoreCase(x)) {
                  // We found the right stack
                  List<Integer> answer = new ArrayList<Integer>();
                  for (Position p : l) {
                     // Be careful that the stack is called in the right order
                     answer.add(dim * p.y + p.x);
                  }
                  return answer;
               }
            }
         }
      }
      return new ArrayList<Integer>();
   }



   private class Position {
      int x;
      int y;

      /** Constructs a Position with coordinates (x,y). */
      public Position(int x, int y) {
         this.x = x;
         this.y = y;
      }

      /** Returns a string representation of this Position. */
      @Override
      public String toString() {
         return "(" + x + ", " + y + ")";
      }

      /** Returns all the neighbors of this Position. */
      public Position[] neighbors() {
         Position[] nbrs = new Position[8];
         int count = 0;
         Position p;
         // generate all eight neighbor positions
         // add to return value if valid
         for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
               if (!((i == 0) && (j == 0))) {
                  p = new Position(x + i, y + j);
                  if (isValid(p)) {
                     nbrs[count++] = p;
                  }
               }
            }
         }
         return Arrays.copyOf(nbrs, count);
      }

   }

   private boolean isValid(Position p) {
      return (p.x >= 0) && (p.x < dim) && (p.y >= 0) && (p.y < dim);
   }

   private boolean isVisited(Position p) {
      // Alternative implementation
      // Does not work with current conditions
      // return stack.contains(p);
      return visited[p.x][p.y];
   }


   private String getWordFromStack(Stack<Position> inputStack) {
      String s = "";
      for (Position p : inputStack) {
         s = s.concat(grid[p.y][p.x]); // Change to p.x, p.y
      }
      return s;
   }

   private void startDfs(Position start) {
      markAllUnvisited();
      stack = new Stack<Position>();
      if (isValid(start)) {
         dfs(start);
      }
   }

   private void dfs(Position p) {
      if (isVisited(p)) {
         return;
      }
      stack.push(p);
      visit(p);
      String currentWord = getWordFromStack(stack).toLowerCase();
      for (Position x : p.neighbors()) {
         if (isValidPrefix(currentWord)) {
            dfs(x);
         }
      }
      if (isValidPrefix(currentWord)) {
         if (currentWord.length() >= minWordLength) {
            if (isValidWord(currentWord)) {
               solutions.add(currentWord.toUpperCase());
               Stack<Position> newStack = new Stack<Position>();
               newStack.addAll(stack);
               locations.add(newStack);
            }
         }
      }
      stack.pop();
      unvisit(p);
   }

   private void visit(Position p) {
      visited[p.x][p.y] = true;
   }

   private void unvisit(Position p) {
      visited[p.x][p.y] = false;
   }

   private void markAllUnvisited() {
      visited = new boolean[dim][dim];
      for (boolean[] row : visited) {
         Arrays.fill(row, false);
      }
   }

}