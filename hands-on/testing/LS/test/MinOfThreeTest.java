import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by kitty on 1/23/18.
 */
public class MinOfThreeTest {
    @Test
    public void min1Boundary() {
        int a = 5;
        int b = 5;
        int c = 5;
        int expected = 5;
        int actual = MinOfThree.min1(a, b, c);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void min1Tie() {
        int a = 5;
        int b = 2;
        int c = 2;
        int expected = 2;
        int actual = MinOfThree.min1(a, b, c);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void min2Boundary() {
        int a = 5;
        int b = 5;
        int c = 5;
        int expected = 5;
        int actual = MinOfThree.min2(a, b, c);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void min2Tie() {
        int a = 5;
        int b = 2;
        int c = 2;
        int expected = 2;
        int actual = MinOfThree.min2(a, b, c);
        Assert.assertEquals(expected, actual);
    }

}